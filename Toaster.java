public class Toaster {
    public int numSlots;
    public int maxTemp;
    public String type;

    public void printType(String type){
        System.out.println("The toaster is of " + type + " type");
    }

    public void toastBread(){
        System.out.println("The toaster is toasting bread");
    }
}