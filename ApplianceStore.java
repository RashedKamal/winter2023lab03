import java.util.Scanner;
public class ApplianceStore {
    public static void main(String[] args){
        Toaster[] toasters = new Toaster[4];
        Scanner reader = new Scanner(System.in);
        for(int i = 0; i < 4; i++){
            Toaster temp = new Toaster();
            System.out.println("Enter number of slots: ");
            temp.numSlots = Integer.parseInt(reader.nextLine());
            System.out.println("Enter maximum temperature value: ");
            temp.maxTemp = Integer.parseInt(reader.nextLine());
            System.out.println("Enter the type of toaster(popup, convection, oven): ");
            temp.type = reader.nextLine();
            toasters[i] = temp;
        }
        System.out.println("The last toaster's number of slots is " + toasters[3].numSlots);
        System.out.println("The last toaster's max temperature is " + toasters[3].maxTemp);
        System.out.println("The last toaster's type is " + toasters[3].type);

        Toaster firstToaster = new Toaster();
        firstToaster.printType(toasters[0].type);
        firstToaster.toastBread();
    }
}